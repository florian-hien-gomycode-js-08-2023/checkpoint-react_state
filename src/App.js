import React, { Component } from "react";
import './App.css'

class App extends Component {
  state = {
    person: {
      fullName: "Israel Adesanya",
      bio: "Israel Adesanya, né le 22 juillet 1989 à Lagos, est un pratiquant nigérian naturalisé néo-zélandais d'arts martiaux mixtes et ancien boxeur et kick-boxeur.",
      imgSrc:
        "https://dmxg5wxfqgb4u.cloudfront.net/styles/athlete_bio_full_body/s3/2023-09/ADESANYA_ISRAEL_L_04-08.png?itok=FOMzxCwD",
      profession: "Pratiquant d'Arts Martiaux Mixtes",
    },
    shows: false,
    time: 0,
  }

  componentDidMount() {
    this.startTime = Date.now()
    this.timerID = setInterval(() => this.tick(), 1000)
  }

  componentWillUnmount() {
    clearInterval(this.timerID)
  }

  tick() {
    this.setState({
      time: Math.floor((Date.now() - this.startTime) / 1000),
    })
  }


  render() {
    let content
    console.log(this.state.shows);
    if (this.state.shows) {
      content = (
        <div style={{width:400, borderRadius: '50px', padding:'1em'}}>
          <div style={{width: '100%', display:'flex',justifyContent:'center'}}>
            <img src={this.state.person.imgSrc} alt="" style={{width:'75%'}} />
          </div>
          <p style={{fontWeight:'bold'}}>Name: <span>{this.state.person.fullName}</span></p>
          <p style={{fontWeight:'bold'}}>Bio: <span>{this.state.person.bio}</span></p>
          <p style={{fontWeight:'bold'}}>Profession: <span>{this.state.person.profession}</span></p>
        </div>
      );  
    }
  
    return (
      <div className="App">
        <div style={{boxShadow:'inset 0 5px 8px #191919, inset 0 -1px 6px #06121d', position:'relative', height:'800px', width:450, marginTop: '70px',borderRadius:'50px'}}>
          <div style={{fontSize:18, display: 'flex',  alignItems: 'center',  justifyContent: 'center', flexDirection:'column'}}>
              <div style={{display:'flex', width:'100%', justifyContent:'center', gap:'30px'}}>
                <button style={{border:'1px solid black', width:'30%', cursor:'pointer', marginTop:'15px', borderRadius:'100px', height:'50px'}} onClick={() => {(this.state.shows) ? (this.setState({shows:false})) : (this.setState({shows:true}))}}>{(this.state.shows) ? ('Show Off') : ('Show')}</button>
                <p style={{width:'30%', textAlign:'center', background:'grey', borderRadius:'100px', fontWeight:'bold', height:'50px', display: 'flex',  alignItems: 'center', justifyContent: 'center'}}>Time: <span>{this.state.time}s</span></p>
              </div>
              {content}
          </div>
        </div>
      </div>
    )
  }
}

export default App
